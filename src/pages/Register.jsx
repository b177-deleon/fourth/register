import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

    const { user } = useContext(UserContext);
    const history = useNavigate();


    const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [phoneNumber, setPhoneNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);


    function registerUser(e){
        e.preventDefault();

        fetch('https://tranquil-bayou-65537.herokuapp.com/users/checkEmail', {
            method: "POST",
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            if(data === true) {
                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Kindly provide another email to complete the registration.'	
                });
            } else {
                fetch('https://tranquil-bayou-65537.herokuapp.com/users/register', {
		                    method: "POST",
		                    headers: {
		                        'Content-Type': 'application/json'
		                    },
		                    body: JSON.stringify({
		                        firstName: firstName,
		                        lastName: lastName,
		                        email: email,
		                        phoneNumber: phoneNumber,
		                        password: password1
		                    })
		                })
		                .then(res => res.json())
		                .then(data => {

		                    console.log(data);

		                    if (data === true) {

		                        // Clear input fields
		                        setFirstName('');
		                        setLastName('');
		                        setEmail('');
		                        setPhoneNumber('');
		                        setPassword1('');
		                        setPassword2('');

		                        Swal.fire({
		                            title: 'Registration successful',
		                            icon: 'success',
		                            text: 'Welcome to Zuitt!'
		                        });

		                        history("/login");

		                    } else {

		                        Swal.fire({
		                            title: 'Something wrong',
		                            icon: 'error',
		                            text: 'Please try again.'   
		                        });
							
		                    }
		                })
            }
        })
        setFirstName('');
        setLastName('');
        setEmail('');
        setPhoneNumber('');
        setPassword1('');
        setPassword2('');
    }

    useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && phoneNumber !== '' && phoneNumber.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, phoneNumber, password1, password2]);

    return ( 

        (user.id !== null) ?
            <Navigate to='/products' />
        
            :

        <Form onSubmit={(e) => registerUser(e)}>
            <h1>Register</h1>
            <Form.Group className="mb-3" controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Enter First Name"
                value={firstName}
                onChange={e => setFirstName(e.target.value)}
                required
                 />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Enter Last Name"
                value={lastName}
                onChange={e => setLastName(e.target.value)}
                required
                 />
            </Form.Group>

            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email Address</Form.Label>
                <Form.Control 
                type="email" 
                placeholder="Enter Email"
                value={email}
                onChange={e => setEmail(e.target.value)}
                required
                 />
            </Form.Group>

            <Form.Group className="mb-3" controlId="phoneNo">
                <Form.Label>Phone Number</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Enter Mobile Number"
                value={phoneNumber}
                onChange={e => setPhoneNumber(e.target.value)}
                required
                 />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                type="password" 
                placeholder="Password"
                value={password1}
                onChange={e => setPassword1(e.target.value)}
                required
                 />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                type="password" 
                placeholder="Verify Password"
                value={password2}
                onChange={e => setPassword2(e.target.value)}
                required
                 />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            :
                <Button variant="primary" type="submit" disabled>
                    Submit
                </Button>
            }
            

            
    </Form>    
    )
}