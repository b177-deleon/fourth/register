import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import {Col, Container} from 'react-bootstrap';
import Products from './Products'
import './Home.css'


export default function Home() {
    const data = {
	    title: "DF COSME",
	    content: "Best Jewelries for All",
	    destination: "/products",
	    label: "Buy now!"
	}

	return (
		<>
				<Banner data={data}/>
				<Products/>	
		</>
	)

}